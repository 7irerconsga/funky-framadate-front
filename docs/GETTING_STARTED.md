# Pour débuter - getting started
Clonez le dépot, installez les dépendances, et lancez le serveur local qui se recharge lorsque vous sauvegardez des fichiers dans le dépot.
Les fois suivantes vous n'aurez qu'a lancer `yarn start`.
 
```bash
# clonez le dépot
git clone https://framagit.org/framasoft/framadate/funky-framadate-front.git
cd funky-framadate-front

# install yarn
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt-get update && sudo apt-get install yarn

yarn install --pure-lockfile
yarn start
# et c'est parti, vous pouvez voir le site de développement http://localhost:4200
```

Pour interagir avec la base de données, il vous faudra également faire démarrer l'API Symfony de backend dans l'autre dépot https://framagit.org/tykayn/date-poll-api ,
qui se lance par défaut sur le port 8000 avec une simple commande (si vous avez installé le tout) `symfony serve`.

# Modifier les configurations par défaut

Les configurations par défaut les plus générales telles que le nom du site ou les adresses d'API sont importées dans les fichiers d'environnement:

[environment.ts par défaut](src/environments/environment.ts)

Les propriétés par défaut des objets sont dans les modèles:

[dossier des modèles](src/app/core/models)

Et c'est le service en charge de la persistence de ces configurations et du stockage local qui surchage les propriétés des modèles quand une personne revient sur le site

[storage.service.ts](src/app/core/services/storage.service.ts)

# Pas de config docker
Nous n'avons pas actuellement de config docker qui solutionnerait tout ça, les merge request sont les bienvenues! :)

=> https://framagit.org/framasoft/framadate/funky-framadate-front
