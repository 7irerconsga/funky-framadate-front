import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Poll } from '../../core/models/poll.model';
import { PollService } from '../../core/services/poll.service';
import { DateService } from '../../core/services/date.service';
import { PollUtilitiesService } from '../../core/services/poll.utilities.service';
import { StorageService } from '../../core/services/storage.service';
import { ApiService } from '../../core/services/api.service';
import { ToastService } from '../../core/services/toast.service';
import { environment } from 'src/environments/environment';
import { Choice } from '../../core/models/choice.model';

@Component({
	selector: 'app-consultation',
	templateUrl: './consultation.component.html',
	styleUrls: ['./consultation.component.scss'],
})
export class ConsultationComponent implements OnInit, OnDestroy {
	public fetching = true;
	public environment = environment;
	private routeSubscription: Subscription;
	window: any;
	public isArchived: boolean;
	public poll: Poll;
	public pollSlug: string;
	public pass_hash: string;
	public maxYesCount: any = 1;
	public detailledDisplay: boolean = false;
	public display_options_menu: boolean = false;
	public show_admin_stuff: boolean = true;
	public favChoicesList: Array<Choice> = [];

	constructor(
		private router: Router,
		private utils: PollUtilitiesService,
		private _Activatedroute: ActivatedRoute,
		public storageService: StorageService,
		public api: ApiService,
		public pollService: PollService,
		public dateService: DateService,
		public toastService: ToastService
	) {}

	/**
	 * fetch poll data on init
	 */
	ngOnInit(): void {
		console.log('constultation de poll');
		this.pollService.poll.subscribe((newpoll: Poll) => {
			this.poll = newpoll;
			if (newpoll) {
				this.isArchived = new Date(newpoll.expiracy_date) < new Date();
				this.poll.choices_grouped.map((elem) => (elem.subSetToYes = false));
				this.findFavouriteChoices(newpoll);
			}
		});

		this._Activatedroute.paramMap.subscribe((params: ParamMap) => {
			console.log('params _Activatedroute', params);
			this.pollSlug = params.get('custom_url');
			this.pass_hash = params.get('pass_hash');

			console.log('this.pass_hash ', this.pass_hash);
			if (this.pass_hash) {
				this.pollService.loadPollByCustomUrlWithPasswordHash(this.pollSlug, this.pass_hash).then((resp) => {
					console.log('loadPollByCustomUrlWithPasswordHash resp', this.pollService._poll.getValue());
					this.fetching = false;
					this.storageService.vote_stack.id = null;
					this.storageService.setChoicesForVoteStack(this.pollService._poll.getValue().choices);
				});
			} else {
				this.pollService.loadPollByCustomUrl(this.pollSlug).then((resp) => {
					console.log('loadPollByCustomUrl resp', resp);
					this.fetching = false;
					this.storageService.vote_stack.id = null;
					this.storageService.setChoicesForVoteStack(this.pollService._poll.getValue().choices);
				});
			}
		});
	}

	ngOnDestroy(): void {
		if (this.routeSubscription) {
			this.routeSubscription.unsubscribe();
		}
	}

	displayOptions() {
		this.display_options_menu = !this.display_options_menu;
	}

	/**
	 * create a	new vote stack
	 */
	addVoteStack(): void {
		this.storageService.vote_stack.custom_url = this.poll.custom_url;
		this.pollService.pass_hash = this.pass_hash;

		this.toastService.display('envoi du vote ....');
		this.api
			.sendNewVoteStackOfPoll(this.storageService.vote_stack)
			.then((resp: any) => {
				console.log('sendNewVoteStackOfPoll resp', resp);
				this.storeVoteStackAndReloadPoll(resp);
			})
			// eslint-disable-next-line @typescript-eslint/unbound-method
			.catch(this.api.ousideHandleError);
	}

	/**
	 * store the updated vote stack
	 * @param voteStack
	 */
	storeVoteStackAndReloadPoll(voteStack: any) {
		if (voteStack.status == 200) {
			this.storageService.mapVotes(voteStack.data);
			this.pollService.enrichVoteStackWithCurrentPollChoicesDefaultVotes(this.storageService.vote_stack);
			if (this.pass_hash) {
				this.pollService.loadPollByCustomUrlWithPasswordHash(this.poll.custom_url, this.pass_hash);
			} else {
				this.pollService.loadPollByCustomUrl(this.poll.custom_url);
			}
		} else {
			this.toastService.display('erreur à l enregistrement');
		}
	}

	private findFavouriteChoices(newpoll: Poll) {
		let tempFavChoiceList = [];
		newpoll.choices.map((choice: Choice) => {
			console.log('choice.score', choice.score, newpoll.max_score);
			if (choice.score === newpoll.max_score) {
				tempFavChoiceList.push(choice);
			}
		});
		this.favChoicesList = tempFavChoiceList;
	}
}
