import { Component, OnInit } from '@angular/core';
import { PollService } from '../../../core/services/poll.service';
import { Poll } from '../../../core/models/poll.model';
import { Subscription } from 'rxjs';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { PollUtilitiesService } from '../../../core/services/poll.utilities.service';
import { StorageService } from '../../../core/services/storage.service';
import { ApiService } from '../../../core/services/api.service';
import { DateService } from '../../../core/services/date.service';
import { ToastService } from '../../../core/services/toast.service';

@Component({
	selector: 'app-consultation-landing',
	templateUrl: './consultation-landing.component.html',
	styleUrls: ['./consultation-landing.component.scss'],
})
export class ConsultationLandingComponent implements OnInit {
	public poll: Poll;
	public pollSlug: string;
	public pass_hash: string;
	public fetching = true;
	public isArchived: boolean;
	public isAdmin: boolean;

	private routeSubscription: Subscription;
	window: any;

	constructor(
		private router: Router,
		private utils: PollUtilitiesService,
		private _Activatedroute: ActivatedRoute,
		public storageService: StorageService,
		public api: ApiService,
		public pollService: PollService,
		public dateService: DateService,
		public toastService: ToastService
	) {}

	/**
	 * fetch poll data on init
	 */
	ngOnInit(): void {
		console.log('constultation de poll');
		this.pollService.poll.subscribe((newpoll: Poll) => {
			this.poll = newpoll;
			if (newpoll) {
				this.isArchived = new Date(newpoll.expiracy_date) < new Date();
				this.poll.is_archived = this.isArchived;
				this.isAdmin = this.poll.admin_key !== null;
				this.poll.choices_grouped.map((elem) => (elem.subSetToYes = false));
			}
		});

		this._Activatedroute.paramMap.subscribe((params: ParamMap) => {
			console.log('params _Activatedroute', params);
			this.pollSlug = params.get('custom_url');
			this.pass_hash = params.get('pass_hash');

			console.log('this.pass_hash ', this.pass_hash);
			if (this.pass_hash) {
				this.pollService.loadPollByCustomUrlWithPasswordHash(this.pollSlug, this.pass_hash).then((resp) => {
					console.log('loadPollByCustomUrlWithPasswordHash resp', resp);
					this.fetching = false;
					this.storageService.vote_stack.id = null;
					this.storageService.setChoicesForVoteStack(this.pollService._poll.getValue().choices);
				});
			} else {
				this.pollService.loadPollByCustomUrl(this.pollSlug).then((resp) => {
					console.log('loadPollByCustomUrl resp', resp);
					this.fetching = false;
					this.storageService.vote_stack.id = null;
					this.storageService.setChoicesForVoteStack(this.pollService._poll.getValue().choices);
				});
			}
		});
	}

	ngOnDestroy(): void {
		if (this.routeSubscription) {
			this.routeSubscription.unsubscribe();
		}
	}
}
