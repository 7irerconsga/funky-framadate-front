import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { SharedModule } from '../../shared/shared.module';
import { AddAnswerComponent } from './add-answer/add-answer.component';
import { AddCommentComponent } from './add-comment/add-comment.component';
import { AnswersComponent } from './answers/answers.component';
import { ParticipationRoutingModule } from './participation-routing.module';
import { ParticipationComponent } from './participation.component';

@NgModule({
	declarations: [ParticipationComponent, AddCommentComponent, AnswersComponent, AddAnswerComponent],
	imports: [CommonModule, ParticipationRoutingModule, SharedModule, TranslateModule.forChild({ extend: true })],
	exports: [],
})
export class ParticipationModule {}
